#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>

const char * cmd = "prog1";

int main() {

    FILE *fp; // używamy metody wysokopoziomowej
    char tekst[] = "MS";
    if ((fp=fopen("bufor.txt", "w"))==NULL) {
        printf ("Nie mogê otworzyæ pliku do zapisu!\n");
        exit(1);
    }
    fprintf (fp, "%s", tekst); // zapisz string
    fclose (fp); /* zamknij plik */

    PROCESS_INFORMATION pi;
    STARTUPINFO si;

    ZeroMemory( & pi, sizeof( pi ) );
    ZeroMemory( & si, sizeof( si ) );
    si.cb = sizeof( si );

    HANDLE sem = CreateSemaphore(NULL,1 , 1, TEXT("Semafor1"));

    if( !CreateProcess( NULL,
    ( LPSTR ) cmd,
    NULL,
    NULL,
    FALSE,
    0,
    NULL,
    NULL,
    & si,
    & pi ) )
    {
        printf( "Could not create process. (%ld)", GetLastError() );
        return 1;
    }



    int F, F1=1, F2=0, F3=1,k=0;
    char proc;
    for(int i = 0; (FALSE || i<100) && k<5 ;){
        WaitForSingleObject(sem, INFINITE);

        if ((fp=fopen("bufor.txt", "r"))==NULL) {
            printf ("Nie mogê otworzyæ pliku do odczytu!\n");
            exit(1);
        }
        fscanf (fp, "%c", &proc);
        fscanf (fp, "%d", &F);
        fclose (fp); /* zamknij plik */
        if(proc=='P'){
            if(F==F3){
                printf("%d\n", F);
                k=0;

                F1=F2;
                F2=F3;
                F3=F1+F2;

                tekst[1] = 'N';
                if ((fp=fopen("bufor.txt", "w"))==NULL) {
                    printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                    exit(1);
                }
                fprintf (fp, "%s", tekst); /* zapisz nasz ³añcuch w pliku */
                fclose (fp); /* zamknij plik */
            }
            else{
                printf("%d error %d\n", F, F3);
                k++;

                tekst[1] = 'R';
                if ((fp=fopen("bufor.txt", "w"))==NULL) {
                    printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                    exit(1);
                }
                fprintf (fp, "%s", tekst); /* zapisz nasz ³añcuch w pliku */
                fclose (fp); /* zamknij plik */
            }
        }
        if(F>(INT_MAX/2)){
            F1=0;
            F2=0;
            F3=1;
            tekst[1] = 'S';
            if ((fp=fopen("bufor.txt", "w"))==NULL) {
                printf ("Nie mogê otworzyæ pliku do zapisu!\n");
                exit(1);
            }
            fprintf (fp, "%s", tekst); /* zapisz nasz ³añcuch w pliku */
            fclose (fp); /* zamknij plik */
        }

        ReleaseSemaphore(sem, 1, NULL);

    }

    WaitForSingleObject(sem, INFINITE);
    if(TRUE){
        tekst[1] = 'E';
        if ((fp=fopen("bufor.txt", "w"))==NULL) {
            printf ("Nie mogê otworzyæ pliku do zapisu!\n");
            exit(1);
        }
        fprintf (fp, "%s", tekst); /* zapisz nasz ³añcuch w pliku */
        fclose (fp); /* zamknij plik */
    }
    ReleaseSemaphore(sem, 1, NULL);

    WaitForSingleObject( pi.hProcess, INFINITE ); //Czekanie na zakoñczenie procesu
    DWORD exitcode;
    if( !GetExitCodeProcess( pi.hProcess, & exitcode ) ) {
        printf( "Could not get process exit code. (%ld)", GetLastError() );
        CloseHandle( pi.hProcess );
        CloseHandle( pi.hThread );
        return 1;
    }
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
    if(exitcode==0){
        printf("Process poprawnie zakonczyl prace");
    }
    else{
        printf( "Process returned: %ld", exitcode );
    }
}
